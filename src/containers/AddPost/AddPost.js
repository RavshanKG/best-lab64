import React from 'react';
import {Button, ControlLabel, FormControl, FormGroup, PageHeader} from "react-bootstrap";
import axios from 'axios';

class AddPost extends React.Component {
    state = {
        title: '',
        value: '',
        date: ''
    };

    AddPost = event => {
        event.preventDefault()
        axios.post('/post.json', {'title': this.state.title, 'value': this.state.value, 'date': new Date()}).then((res) => {
            this.props.history.push('/');
        })
    };
    handleChange = event => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    };

    render() {
        return (
            <div className="container">
                <form onSubmit={this.AddPost}>
                    <FormGroup>
                        <PageHeader>Add new post</PageHeader>
                        <ControlLabel>Title</ControlLabel>
                        <FormControl
                            type="text"
                            name="title"
                            value={this.state.title}
                            placeholder="Enter text"
                            onChange={this.handleChange}
                        />
                        <FormControl
                            type="text"
                            name='value'
                            value={this.state.value}
                            placeholder="Enter text"
                            onChange={this.handleChange}
                        />
                        <Button type={'submit'}>Save</Button>
                    </FormGroup>
                </form>
            </div>
        )
    }
}

export default AddPost;
