import React, { Component } from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Header from "../../components/Header/Header";
import PostList from "../PostList/PostList";
import AddPost from "../AddPost/AddPost";
import OpenPost from "../../components/OpenPost/OpenPost";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
          <div className="App">
            <Header/>
          <Switch>
              <Route path='/' exact component={PostList} />
              <Route path='/add' exact component={AddPost} />
              <Route path="/post/:id" exact component={OpenPost}/>
          </Switch>
          </div>
      </BrowserRouter>
    );
  }
}

export default App;
