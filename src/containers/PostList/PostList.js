import React from 'react';
import axios from 'axios';
import Post from "../../components/Post/Post";

class PostList extends React.Component {
    state = {
        postList: []
    };

    pushPostInState = (value, id) => {
        const postList = [...this.state.postList];
        console.log(value);
        postList.push({'data': value, 'id': id});
        this.setState({postList})
    }



    componentDidMount () {
        axios.get('/post.json').then(res => {
            console.log(res.data);

            for (let key in res.data){
                this.pushPostInState(res.data[key], key)
            }
        })
    }

    render() {
        console.log(this.state.postList)
        return(
            <div className="container">
                {this.state.postList.map(value => <Post title={value.data.title} text={value.data.value} date={value.data.date} id={value.id} /> )}
            </div>
        );
    }
}

export default PostList;
