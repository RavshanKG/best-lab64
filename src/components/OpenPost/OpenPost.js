import React from 'react';
import {Button, Panel} from "react-bootstrap";
import axios from "axios/index";
import Moment from 'react-moment';
import {LinkContainer} from "react-router-bootstrap";

class OpenPost extends React.Component {
  state = {
    data: {}
  };

  componentDidMount() {
    axios.get(`/post/${this.props.match.params.id}.json`).then(res => {
      this.setState({data: res.data});
    })
  }

  removePost = () => {
    axios.delete(`/post/${this.props.match.params.id}.json`).then(res => {
      this.props.history.push('/');
    })
  };


  render() {
    return (
      <div>
        <Panel bsStyle="primary">
          <Panel.Heading><Moment format="YYYY-MM-DD HH:mm">{this.state.data.date}</Moment></Panel.Heading>
          <Panel.Body>{this.state.data.title}</Panel.Body>
          <Panel.Footer>{this.state.data.value}</Panel.Footer>
        </Panel>
        <Button onClick={this.removePost}>remove</Button><LinkContainer to={`/post/${this.props.match.params.id}/edit`}><Button>edit</Button></LinkContainer>
      </div>
    )
  }
}

export default OpenPost;