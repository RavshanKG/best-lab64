import React from 'react';
import {Button, MenuItem, Nav, Navbar, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Header = () => {
    return (
        <Navbar inverse>
            <Navbar.Header>
                <Navbar.Brand>
                    My blog
                </Navbar.Brand>
                <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
                <Nav pullRight>
                    <LinkContainer to='/' exact><Button bsStyle="primary">Home</Button></LinkContainer>
                    <LinkContainer to='/add'><Button bsStyle="primary">Add</Button></LinkContainer>
                    <LinkContainer to='/about'><Button bsStyle="primary">About</Button></LinkContainer>
                    <LinkContainer to='/contacts'><Button bsStyle="primary">Contacts</Button></LinkContainer>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

export default Header;
