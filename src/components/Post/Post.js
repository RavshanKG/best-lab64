import React from 'react';
import Panel from "react-bootstrap/es/Panel";
import Moment from 'react-moment';
import './Post.css'
import {NavLink} from "react-router-dom";

const Post = (props) => {
    console.log(props)
    return (
        <div className="container">
            <Panel bsStyle="primary">
                <Panel.Heading><Moment format="YYYY-MM-DD HH:mm">{props.date}</Moment></Panel.Heading>
                <Panel.Body><NavLink to={'post/' + props.id}>{props.title}</NavLink></Panel.Body>
                <Panel.Footer>{props.text}</Panel.Footer>
            </Panel>
        </div>
    )
}

export default Post;