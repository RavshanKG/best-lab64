import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App';
import registerServiceWorker from './registerServiceWorker';
import axios from "axios";

axios.defaults.baseURL = 'https://best-lab64.firebaseio.com/';
ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
